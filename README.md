# Ansible playbook for setting up a git hook boilerplate for vault decryption of notebooks
Run as `ansible-playbook -i "localhost," path/to/setup.yml --connection=local 
--extra-vars "vault_key=ansible.vault.bla.bla.bla"` from the target directory


## Assumptions
- Requires ansible and ansible-vault to be present in the system python/default python 
  interpreter
- Requires lastpass-cli to be installed and present as `lpass` somewhere on the path
- Requires the user to be logged in to lastpass via `lpass login <email>` and having
  the agent running while using the git hooks