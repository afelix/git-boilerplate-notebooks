---
- name: Get local pwd
  command: "echo {{ lookup('env', 'PWD') }}"
  register: pwd

- name: Check if required vault_key variable is present
  fail:
    msg: "'vault_key' is a required variable and needs to be set at run time using --extra-vars"
  when: vault_key is undefined

- name: Initialise git
  command: git init
  args:
    chdir: "{{ pwd.stdout }}"

- name: Create notebooks and scripts directories
  file:
    path: "{{ '/'.join((pwd.stdout, item)) }}"
    state: directory
  with_items:
    - notebooks
    - .scripts/vars

- name: Write vars template
  template:
    src: external_vars.yml.j2
    dest: "{{ '/'.join((pwd.stdout, '.scripts', 'vars', 'external_vars.yml')) }}"

- name: Copy the scripts
  copy:
    src: "{{ item }}"
    dest: "{{ '/'.join((pwd.stdout, '.scripts')) }}"
  with_items:
    - play_decrypt.yml
    - play_encrypt.yml
    - post-merge
    - pre-commit

- name: Copy the gitignore
  copy:
    src: .gitignore
    dest: "{{ pwd.stdout }}"

- name: Set up the git hook for encrypting notebooks
  copy:
    src: pre-commit
    dest: "{{ '/'.join((pwd.stdout, '.git', 'hooks', 'pre-commit')) }}"
    mode: u+rwx

- name: Set up the git hook for decrypting notebooks
  copy:
    src: post-merge
    dest: "{{ '/'.join((pwd.stdout, '.git', 'hooks', 'post-merge')) }}"
    mode: u+rwx

- name: Set up git hook to decrypt notebook after committing for direct continued usage
  file:
    src: "{{ '/'.join((pwd.stdout, '.git', 'hooks', 'post-merge'))}}"
    dest: "{{ '/'.join((pwd.stdout, '.git', 'hooks', 'post-commit')) }}"
    state: link

- name: Keep the notebooks folder
  file:
    path: "{{ '/'.join((pwd.stdout, 'notebooks', '.gitkeep')) }}"
    state: touch

- name: Store the scripts in the repository
  command: "git add {{ item }}"
  args:
    chdir: "{{ pwd.stdout }}"
  with_items:
    - .scripts/play_decrypt.yml
    - .scripts/play_encrypt.yml
    - .scripts/vars/external_vars.yml
    - .scripts/post-merge
    - .scripts/pre-commit
    - notebooks/.gitkeep
    - .gitignore

- name: Commit the boilerplate
  command: git commit -m "boilerplate"
  args:
    chdir: "{{ pwd.stdout }}"
